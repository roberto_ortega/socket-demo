var socketIO = require('socket.io');

module.exports = function(server) {
  var io = socketIO(server);
  
  io.on('connection', function (socket) {
    socket.on('add image', function (url) {
      io.emit('display image', url);
    });
  });

}

