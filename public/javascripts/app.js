var socket = io();

socket.on('display image', function(url) {
  appendImage(url);
});

document.querySelector('button').addEventListener('click', function () {
  let url = getInputValue();
  socket.emit('add image', url);
});

function getInputValue() {
  let input = document.querySelector('input');
  let value = input.value;
  input.value = "";
  return value
}

function appendImage(url) {
  let image = `<img style="height: 300px" class="col-md-6" src="${url}">`
  document.querySelectorAll('.row')[0].insertAdjacentHTML('beforeend', image)
}
